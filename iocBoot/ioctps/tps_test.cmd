#!../../bin/linux-x86_64/tps

## You may have to change tps to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/tps.dbd"
tps_registerRecordDeviceDriver pdbbase

epicsEnvSet ("STREAM_PROTOCOL_PATH", "${TOP}/db")
epicsEnvSet ("PORT", "L1")
epicsEnvSet ("IOCBL", "LPQ")
epicsEnvSet ("IOCDEV", "tp")

drvAsynIPPortConfigure("${PORT}","127.0.0.1:50123",0,0,0)
#drvAsynSerialPortConfigure("${PORT}","/dev/$(DEVPORT)",0,0,0)
#asynSetOption("L1", -1, "baud", "9600")
#asynSetOption("L1", -1, "bits", "8")
#asynSetOption("L1", -1, "parity", "none")
#asynSetOption("L1", -1, "stop", "1")
#asynSetOption("L1", -1, "clocal", "N")
#asynSetOption("L1", -1, "crtscts", "N")
##asynOctetSetInputEos("L1",0,"\r")
#asynOctetSetOutputEos("L1",0,"\r")

## Load record instances
dbLoadRecords("${EXSUB}/db/exsub.db","P=$(IOCBL):,R=$(IOCDEV):exsub")
dbLoadRecords("${ASYN}/db/asynRecord.db","P=$(IOCBL):,R=$(IOCDEV):asyn,PORT=${PORT},ADDR=0,IMAX=256,OMAX=256")

dbLoadRecords("db/tc110.db","P=$(IOCBL):,R=$(IOCDEV)1:,SERADDR_PV=001,ASYNPORT=${PORT}")
#dbLoadRecords("db/tc110.db","P=$(IOCBL):,R=$(IOCDEV)2:,SERADDR_PV=002,ASYNPORT=${PORT}")
#dbLoadRecords("db/tc110.db","P=$(IOCBL):,R=$(IOCDEV)3:,SERADDR_PV=003,ASYNPORT=${PORT}")
#dbLoadRecords("db/tc110.db","P=$(IOCBL):,R=$(IOCDEV)4:,SERADDR_PV=004,ASYNPORT=${PORT}")
#dbLoadRecords("db/tc110.db","P=$(IOCBL):,R=$(IOCDEV)5:,SERADDR_PV=005,ASYNPORT=${PORT}")
#dbLoadRecords("db/tc110.db","P=$(IOCBL):,R=$(IOCDEV)6:,SERADDR_PV=006,ASYNPORT=${PORT}")
#dbLoadRecords("db/tc110.db","P=$(IOCBL):,R=$(IOCDEV)7:,SERADDR_PV=007,ASYNPORT=${PORT}")
#dbLoadRecords("db/tc110.db","P=$(IOCBL):,R=$(IOCDEV)8:,SERADDR_PV=008,ASYNPORT=${PORT}")


cd "${TOP}/iocBoot/${IOC}"

#set_savefile_path("/EPICS/autosave")
#set_pass0_restoreFile("auto_settings.sav")
#set_pass1_restoreFile("auto_settings.sav")

iocInit

#create_monitor_set("auto_settings.req", 30, "P=$(IOCBL),R=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
