# LPS-tps

Container with IOC for Pfeiffer Turbo Pump controllers TC110

## Docker-compose.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-containers/lps-tps/lpstps:latest
    container_name: tps
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: “/EPICS/IOCs/lps-tps/iocBoot/ioctps”
    command: "./tps.cmd"
    volumes:
      - type: bind
        source: “/EPICS/autosave/tps”
        target: “/EPICS/autosave”
    devices:
      - /dev/ttyUSB0:/dev/ttyUSB0
    environment:
      - DEVPORT=ttyUSB0
      - IOCBL=LPQ
      - IOCDEV=tp

```
